import { Module } from '@nestjs/common';
import { NaiveBayesClassifierModule } from "./modules";

@Module({
  imports: [ NaiveBayesClassifierModule ],
  controllers: [],
  providers: [],
})
export class AppModule {
}
