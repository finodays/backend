import { Module } from "@nestjs/common";
import { NaiveBayesClassifierController } from "./naive-bayes-classifier.controller";
import { NaiveBayesClassifierService } from "./naive-bayes-classifier.service";

@Module({
  imports: [],
  providers: [ NaiveBayesClassifierService ],
  controllers: [ NaiveBayesClassifierController ],
  exports: [],
})
export class NaiveBayesClassifierModule {
}
