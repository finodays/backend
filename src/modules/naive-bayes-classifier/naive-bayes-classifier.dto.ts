import { IsNumber, IsString } from "class-validator";

export class GetTextEmotionalScoreRequestDTO {
  @IsString()
  text!: string;
}

export class GetTextEmotionalScoreResponseDTO {
  emotionalScore!: EmotionalScores;
}

export enum EmotionalScores {
  POSITIVE = 'POSITIVE',
  NEGATIVE = 'NEGATIVE',
  NEUTRAL = 'NEUTRAL',
}

export class LearnNaiveBayesClassifierRequestDTO {
  @IsString()
  text!: string;

  @IsNumber()
  recognitionScore!: number;
}
