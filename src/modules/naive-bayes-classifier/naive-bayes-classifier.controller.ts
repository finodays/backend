import { Body, Controller, Post } from "@nestjs/common";
import {
  GetTextEmotionalScoreRequestDTO,
  GetTextEmotionalScoreResponseDTO,
  LearnNaiveBayesClassifierRequestDTO
} from "./naive-bayes-classifier.dto";
import { NaiveBayesClassifierService } from "./naive-bayes-classifier.service";

@Controller()
export class NaiveBayesClassifierController {
  constructor(
    private readonly naiveBayesClassifierService: NaiveBayesClassifierService,
  ) {
  }

  @Post('learnNaiveBayesClassifier')
  async learnNaiveBayesClassifier(
    @Body() data: LearnNaiveBayesClassifierRequestDTO,
  ): Promise<void> {
    await this.naiveBayesClassifierService.learnNaiveBayesClassifier(data);
  }

  @Post('getTextEmotionalScore')
  async getTextEmotionalScore(
    @Body() data: GetTextEmotionalScoreRequestDTO,
  ): Promise<GetTextEmotionalScoreResponseDTO> {
    return await this.naiveBayesClassifierService.getTextEmotionalScore(data);
  }
}
