import { HttpException, Injectable } from "@nestjs/common";
import {
  EmotionalScores,
  GetTextEmotionalScoreRequestDTO,
  GetTextEmotionalScoreResponseDTO,
  LearnNaiveBayesClassifierRequestDTO,
} from "./naive-bayes-classifier.dto";
import LanguageDetect from 'languagedetect';
import {
  transferPlainEmotionalScoreToEnum,
  transferRecognitionScoreToEmotionalScore
} from "./naive-bayes-classifier.utils";
import { exec } from "child_process";

const bayes = require('bayes');

@Injectable()
export class NaiveBayesClassifierService {
  private readonly lngDetector = new LanguageDetect();
  private readonly englishBayes: any;
  private readonly russianBayes: any;
  constructor() {
    this.englishBayes = bayes();
    this.russianBayes = bayes();
    this.startLearningNaiveBayes('english');
    this.startLearningNaiveBayes('russian');
  }

  async getTextEmotionalScore(
    { text }: GetTextEmotionalScoreRequestDTO,
  ): Promise<GetTextEmotionalScoreResponseDTO> {
    const detectedLanguage = this.getDetectedLanguage(text);
    return {
      emotionalScore: await this.bayesCategorize(detectedLanguage, text),
    };
  }

  async learnNaiveBayesClassifier(
    {
      text,
      recognitionScore,
    }: LearnNaiveBayesClassifierRequestDTO,
  ): Promise<void> {
    const emotionalScore = transferRecognitionScoreToEmotionalScore(recognitionScore);
    const detectedLanguage = this.getDetectedLanguage(text);
    switch (detectedLanguage) {
      case 'english':
        await this.russianBayes.learn(text, emotionalScore);
        break;
      case 'russian':
        await this.englishBayes.learn(text, emotionalScore);
        break;
      default:
        throw new HttpException('Язык вашего сообщения не распознан, попробуйте снова', 500);
    }
  }

  async bayesCategorize(lang: string, text: string): Promise<EmotionalScores> {
    switch (lang) {
      case 'english':
        return await this.getEnglishBayesCategorize(text);
      case 'russian':
        return await this.getRussianBayesCategorize(text);
      default:
        throw new HttpException('Язык вашего сообщения не распознан, попробуйте снова', 500);
    }
  }

  async getEnglishBayesCategorize(text: string): Promise<EmotionalScores> {
    const emotionalScore = await this.englishBayes.categorize(text);
    return transferPlainEmotionalScoreToEnum(emotionalScore);
  }

  async getRussianBayesCategorize(text: string): Promise<EmotionalScores> {
    const emotionalScore = await this.russianBayes.categorize(text);
    return transferPlainEmotionalScoreToEnum(emotionalScore);
  }

  getDetectedLanguage(text: string): string {
    const detectedLanguages = {} as any;

    this.lngDetector.detect(text).forEach(i => {
      switch (i[0]) {
        case 'russian':
          detectedLanguages.russian = i[1];
          break;
        case 'english':
          detectedLanguages.english = i[1];
          break;
        default:
          break;
      }
    });

    return Object.keys(detectedLanguages).reduce(
      function(a, b) {
        return detectedLanguages[a] > detectedLanguages[b] ? a : b
      }
    );
  }

  startLearningNaiveBayes(language: string): void {
    if (language === 'english') {
      exec('ls', { cwd: './' }, (stdout) => {
        console.log('stdout :>> ', stdout);
      });
      // this.englishBayes.learn(text, EmotionalScores.NEGATIVE);
      // this.englishBayes.learn(text, EmotionalScores.POSITIVE);
      // this.englishBayes.learn(text, EmotionalScores.NEUTRAL);
    }
  }
}
