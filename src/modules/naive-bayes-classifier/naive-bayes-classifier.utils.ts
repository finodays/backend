import { EmotionalScores } from "./naive-bayes-classifier.dto";
import { HttpException } from "@nestjs/common";

export function transferPlainEmotionalScoreToEnum(emotionalScore: string): EmotionalScores {
  switch (emotionalScore) {
    case 'positive':
      return EmotionalScores.POSITIVE;
    case 'negative':
      return EmotionalScores.NEGATIVE;
    case 'neutral':
      return EmotionalScores.NEUTRAL;
    default:
      throw new HttpException('Алгоритм не смог дать однозначного ответа, попробуйте снова', 500);
  }
}

export function transferRecognitionScoreToEmotionalScore(recognitionScore: number): EmotionalScores {
  switch (recognitionScore) {
    case 1:
      return EmotionalScores.NEGATIVE;
    case 2:
      return EmotionalScores.NEUTRAL;
    case 3:
      return EmotionalScores.POSITIVE;
    default:
      throw new HttpException(`Алгоритм не сможет обучиться с такой ${ recognitionScore } запрашиваемой оценкой`, 500);
  }
}
