import { NestFactory } from '@nestjs/core';
import * as bodyParser from 'body-parser';
import { AppModule } from './app.module';
import { HttpException, ValidationPipe } from "@nestjs/common";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(bodyParser.json({limit: '5mb'}));
  app.use(bodyParser.urlencoded({limit: '5mb', extended: true}));
  app.enableCors({ origin: "*" });

  app.setGlobalPrefix('api');
  app.useGlobalPipes(new ValidationPipe({
    transform: true,
    exceptionFactory: errors => {
      const list = errors.map(error => {
        return error.toString();
      });
      console.error(`Validation errors: ` + list.join(' - '));
      const { value, property } = errors[0];
      throw new HttpException(`Значение ${ value } поля ${ property } не валидно`, 500);
    },
  }));
  const port = process.env.PORT || '3030';
  await app.listen(port);
  console.log('Server started on port ' + port);
}

bootstrap().catch(e => {
  console.error('Error on server start: ' + e);
  process.exit(1);
});
